package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Logger.SetLevel(log.DEBUG)
	/*
	e.GET("/", index)
	e.GET("/restservices/clds/v2/templates/", getTemplates)
	e.GET("/restservices/clds/v2/policyToscaModels/", getPolicies)

	e.GET("/restservices/clds/v2/loop/:loopID", updateLoopDetails)
	e.POST("/restservices/clds/v2/loop/create/:loopID?templateName=:templateName", createLoopInstance)//must review this 
	e.PUT("/restservices/clds/v2/loop/addOperationaPolicy/:loopID/policyModel/:policyType/:policyVersion", addOperationaPolicy)
	e.PUT("/restservices/clds/v2/loop/removeOperationaPolicy/:loopID/policyModel/:policyType/:policyVersion", removeOperationaPolicy)
	e.POST("/restservices/clds/v2/loop/updateMicroservicePolicy/:loopID", addTcaConfig)
	e.POST("/restservices/clds/v2/loop/updateOperationalPolicies/:loopID", addOperationalPolicyConfig)
	e.PUT("/restservices/clds/v2/loop/:action/:loopID", putLoopPolicyAction)//policy engine part
	e.PUT("/restservices/clds/v2/loop/:action/:loopID", putMicroserviceAction)//DCAE part
	e.POST("/reset", reset)
	generateInitialTemplateList()
	generateInitialPolicyList()
	generateInitialLoopInstances()
	*/
	e.Logger.Fatal(e.Start(":30258"))
}

//LoopID == LoopName
//We can gather putLoopPolicyAction and putMicroserviceAction in 1 function
//policy action in (submit, stop, restart)
//DCAE action in (deploy, stop)
//must create stuctures policy/template/loop_details !!!!!!
