package main

import (
	//"net/http"

	//"github.com/labstack/echo"
)

//describes componenet state (policy states != DCAE states)
type State struct {
	ComponentState  struct{
		StateName	string 	`json:"stateName"`
	}	`json:"componentState"`
}

type Resource struct {
	ResourceID  struct{
		VfModuleModelName				string 	`json:"vfModuleModelName"`
		VfModuleModelInvariantUUID		string 	`json:"vfModuleModelInvariantUUID"`
		VfModuleModelUUID				string 	`json:"vfModuleModelUUID"`
		VfModuleModelVersion			string 	`json:"vfModuleModelVersion"`
		VfModuleModelCustomizationUUID	string 	`json:"vfModuleModelCustomizationUUID"`
	}	`json:"VfModule.ID"`
}

//to adjust 
type OperationalPolicy struct {
	Name	string 	`json:"name"`
}

//to adjust 
type MicroServicePolicy struct {
	Name	string 	`json:"name"`
}

//LoopDetails describes loop innstance in CLAMP
type LoopDetails struct {
	Name 			string 	`json:"name"`
	Components      struct{
		POLICY      State   `json:"POLICY"`
		DCAE		State   `json:"DCAE"`
	}
	ModelService  struct{
		ResourceDetails   struct{
			VFModule   struct{Resource} 	`json:"VFModule"`
		}	`json:"resourceDetails"`
	}	`json:"modelService"`
    OperationalPolicies     []OperationalPolicy	    `json:"operationalPolicies"`
    MicroServicePolicies    []MicroServicePolicy	`json:"microServicePolicies"`	
}
