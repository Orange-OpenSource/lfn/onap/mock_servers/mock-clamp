package main

import (
	//"net/http"

	//"github.com/labstack/echo"
)


//describes loop template in Clamp database
type Template struct {
	Name 			string 	`json:"name"`
	ModelService 	struct{
		ServiceDetails  struct{
			Name  string `json:"name"`
		} `json:"serviceDetails"`
	} 	`json:"modelService"`
}

//describes policy in Clamp database
type Policy struct {
	PolicyModelType   string    `json:"policyModelType"`
	Version           string 	`json:"version"`
	PolicyAcronym     string    `json:"policyAcronym"`
	CreatedDate       string    `json:"createdDate"`
	UpdatedDate       string    `json:"updatedDate"`
	UpdatedBy         string    `json:"updatedBy"`
	CreatedBy         string    `json:"createdBy"`
}

var templateList []Template
/*
func generateInitialTemplateList() {
	templateList = nil;
	templateList = append(templateList, Template{
		Name: 			"template_service01",
		ModelService:	struct {
			ServiceDetails:   struct {
				Name:	"test_service01"},},},)

	templateList = append(templateList, Template{
		Name: 			"template_service02",
		ModelService:	struct {
			ServiceDetails:  struct {
				Name:	"test_service02",
			},},},)
}
*/

var policyList []Policy

func generateInitialPolicyList() {
	policyList = nil;
	policyList = append(policyList, Policy{
		PolicyModelType:	"onap.policies.controlloop.MinMax",
		Version:			"1.0.0",
		PolicyAcronym:		"MinMax",
		CreatedDate:		"2020-04-30T09:03:30.362897Z",
		UpdatedDate:		"2020-04-30T09:03:30.362897Z",
		UpdatedBy:			"Not found",
		CreatedBy:			"Not found",
	},)
	policyList = append(policyList, Policy{
		PolicyModelType:	"onap.policies.controlloop.Guard",
		Version:			"1.0.0",
		PolicyAcronym:		"Guard",
		CreatedDate:		"2020-04-30T09:03:30.362897Z",
		UpdatedDate:		"2020-04-30T09:03:30.362897Z",
		UpdatedBy:			"Not found",
		CreatedBy:			"Not found",
	},)
}
